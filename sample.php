<?php

// Choose which database you would like to perform
// 1 = Create table, 2 = Insert data, 3 = Select data, 4 = Delete data, 5 = Update data
$databasequery = 1;

// Where to store the logs
$errorlog   = "errorlogs/errorlog-" . date("Y-m-d") . ".txt";
$successlog = "successlogs/successlog-" . date("Y-m-d") . ".txt";
$logs       = [
    'ErrorLog'   => $errorlog,
    'SuccessLog' => $successlog
];

// Initialize configs
$servername = "127.0.0.1";
$username   = "root";
$password   = "root";
$dbname     = "mydb";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);

// Check connection
if ( ! $conn) {
    $message = "Connection failed: " . mysqli_connect_error() . "\n";
    writeToErrorLog($message, $errorlog);
    die();
}

// Create database table
if (1 == $databasequery) {
    // Execute the create
    createDatabaseTable($conn, $logs);
}

// Create database entry
if (2 == $databasequery) {
    $inputdata = [
        'FirstName'   => 'John',
        'LastName'    => 'Doe',
        'PhoneNumber' => '2034987432',
        'Email'       => 'johnd@test.com',
        'Address'     => '1112 Main Street',
        'City'        => 'Stratford',
        'State'       => 'NY',
        'ZipCode'     => '12345'
    ];

    // Execute the insert
    insertData($conn, $logs, $inputdata);
}

// Select data from database, can be any number of fields that are in the table
if (3 == $databasequery) {
    $pickeddata = [
        'clientid',
        'firstname',
        'lastname',
        'phonenumber',
        'signondate'
    ];

    // Put array in comma separated string
    $pickdata = rtrim(implode(', ', $pickeddata), ',');

    // Execute the select
    selectData($conn, $logs, $pickdata);
}

// Delete record from database, can have multiple field values to look at or just one
if (4 == $databasequery) {
    $removedata = [
        'clientid' => '4',
        'lastname' => 'Doe'
    ];

    // If only one value then only have one where clause, but if mulitple then you need more
    if (count($removedata) == 1) {
        foreach ($removedata as $field => $value) {
            $clause = $field . ' = ' . $value;
        }
    } else {
        foreach ($removedata as $field => $value) {
            $clause .= $field . ' = "' . $value . '" and ';
        }
    }

    // Trim the last 'and' off the where clause
    $clause = rtrim($clause, ' and ');

    // Execute the delete
    deleteData($conn, $logs, $clause);
}

// Update record from database, can have multiple fields to update for multiple clauses
if (5 == $databasequery) {

    // Record to update in DB
    $recordtoupdate = [
        'email' => 'example@example.com'
    ];

    // What values to lookfor
    $whereclause = [
        'city' => 'Stratford'
    ];

    // Get the field name and values you need to update
    foreach ($recordtoupdate as $newfield => $newvalue) {
        $setvalue = $newfield . ' = "' . $newvalue . '"';
    }

    // Get the where clause where you need to look at
    foreach ($whereclause as $wherefield => $wherevalue) {
        $clause = $wherefield . ' = "' . $wherevalue . '"';
    }

    // Execute the update
    updateData($conn, $logs, $setvalue, $clause);
}

// Function to create a database table
function createDatabaseTable($conn, $logs)
{

    $query = "CREATE TABLE Clients (
        clientid INT(7) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        firstname VARCHAR(30) NOT NULL,
        lastname VARCHAR(30) NOT NULL,
        phonenumber CHAR(10),
        email VARCHAR(40),
        address VARCHAR(40),
        city VARCHAR (40),
        state VARCHAR(40),
        zipcode VARCHAR(5),
        signondate TIMESTAMP
        )";

    // DB table was created successfully
    if (mysqli_query($conn, $query)) {
        $message = "Table Clients created successfully. \n";
        writeToSuccessLog($message, $logs['SuccessLog']);
        return;
    }

    // DB create table had an error
    $message = "Error creating table: " . mysqli_error($conn) . ". \n";
    writeToErrorLog($message, $logs['ErrorLog']);

    // Close connection
    $conn->close();
}

// Insert record into database using Prepared statements
function insertData($conn, $logs, $inputdata)
{

    // prepare and bind
    $stmt = $conn->prepare("INSERT INTO Clients (firstname, lastname, phonenumber, email, address, city, state, zipcode) VALUES (?, ?, ?, ?, ?, ?, ?,?)");

    if ( ! $stmt->bind_param("ssssssss", $inputdata['FirstName'], $inputdata['LastName'], $inputdata['PhoneNumber'], $inputdata['Email'], $inputdata['Address'], $inputdata['City'], $inputdata['State'], $inputdata['ZipCode'])) {
        $message = "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error . " \n";
        writeToErrorLog($message, $logs['ErrorLog']);
        return;
    }

    // Excute the prepared statement
    if ( ! $stmt->execute()) {
        $message = "Execute failed: (" . $stmt->errno . ") " . $stmt->error . " \n";
        writeToErrorLog($message, $logs['ErrorLog']);
        return;
    }

    // Create successful log message with data outputted
    $message = "New record created successfully. \n";
    $message .= print_r($inputdata, true);
    writeToSuccessLog($message, $logs['SuccessLog']);

    // Close connection
    $stmt->close();
    $conn->close();
}

// Select records from a database
function selectData($conn, $logs, $pickdata)
{

    $query  = "SELECT $pickdata FROM Clients";
    $result = mysqli_query($conn, $query);

    // Output data you have requested
    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {

            // Create successful log message with data found
            $message = "New record found successfully. \n";
            $message .= print_r($row, true);
            writeToSuccessLog($message, $logs['SuccessLog']);
        }
    } else {
        // Create successful log message even if no data found
        $message = "No records found at this time. \n";
        writeToSuccessLog($message, $logs['SuccessLog']);
    }

    // Close connection
    mysqli_close($conn);
}

// Delete records from a database
function deleteData($conn, $logs, $clause)
{

    $query = "DELETE FROM Clients WHERE $clause";

    if (mysqli_query($conn, $query)) {
        // Create successful log message with data deleted
        $message = "Record deleted successfully. \n";
        writeToSuccessLog($message, $logs['SuccessLog']);
        return;
    }

    // Create error message
    $message = "Error deleting record: " . mysqli_error($conn) . "\n";
    writeToErrorLog($message, $logs['ErrorLog']);

    // Close connection
    mysqli_close($conn);
}

// Update record from a database
function updateData($conn, $logs, $setvalue, $clause)
{

    $query = "UPDATE Clients SET $setvalue WHERE $clause";

    if (mysqli_query($conn, $query)) {
        // Create successful log message with data updated
        $message = "Record updated successfully. \n";
        writeToSuccessLog($message, $logs['SuccessLog']);
        return;
    }

    // Error message
    $message = "Error updating record: " . mysqli_error($conn) . "\n";
    writeToErrorLog($message, $logs['ErrorLog']);

    mysqli_close($conn);
}

// If error occurs, write to error log
function writeToErrorLog($message, $errorlog)
{
    error_log(date("h:i:sa") . " - " . $message, 3, $errorlog);
}

// If success occurs, write to success log
function writeToSuccessLog($message, $successlog)
{
    file_put_contents($successlog, date("h:i:sa") . " - " . $message, FILE_APPEND);
}
